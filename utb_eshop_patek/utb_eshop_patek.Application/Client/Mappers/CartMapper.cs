﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Carts;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Application.Client.Mappers
{
    public class CartMapper : ICartMapper
    {
        private readonly IMapper _mapper;

        public CartMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<CartItemViewModel> GetViewModelsFromEntities(IList<CartItem> entities)
        {
            return _mapper.Map<IList<CartItemViewModel>>(entities);
        }
    }
}
