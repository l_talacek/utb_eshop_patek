﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Client.ViewModels.Orders
{
    public class IndexViewModel : BaseViewModel
    {
        public IList<OrderViewModel> Orders { get; set; }
    }
}
