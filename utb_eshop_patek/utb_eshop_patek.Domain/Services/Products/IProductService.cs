﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Domain.Services.Products
{
    public interface IProductService
    {
        List<Product> GetAll();
        Product Get(Func<Product, bool> predicate);
        Product Insert(Product product);
        Product Delete(Product product);
        Product Update(Product product);
    }
}
