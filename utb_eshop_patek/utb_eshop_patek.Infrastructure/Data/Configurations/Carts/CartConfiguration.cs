﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Infrastructure.Data.Configurations.Carts
{
    public class CartConfiguration : IEntityTypeConfiguration<Cart>
    {
        public void Configure(EntityTypeBuilder<Cart> builder)
        {
            builder.ToTable("Cart", "Web");
            builder.HasKey(e => e.ID);
            builder.HasMany(e => e.CartItems).WithOne(e => e.Cart).IsRequired().HasForeignKey(e => e.CartID).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
