﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Orders;

namespace utb_eshop_patek.Application.Admin.ApplicationServices.Orders
{
    public interface IOrderApplicationService
    {
        void CreateOrder(int userID, string userTrackingCode);
        IndexViewModel GetOrders(int userID);
    }
}
