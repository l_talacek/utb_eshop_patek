﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using utb_eshop_patek.Application.Admin.ApplicationServices.Security;
using utb_eshop_patek.Application.Admin.ViewModels.Security;
using utb_eshop_patek.Areas.Admin.Controllers.Common;
using utb_eshop_patek.Domain.Constants;

namespace utb_eshop_patek.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class SecurityController : AdminController
    {
        private readonly ISecurityApplicationService _securityApplicationService;

        public SecurityController(ISecurityApplicationService securityApplicationService)
        {
            _securityApplicationService = securityApplicationService;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            var result = await _securityApplicationService.Login(viewModel);

            if (result)
            {
                var user = await _securityApplicationService.GetUserByEmail(viewModel.Login);
                var roles = await _securityApplicationService.GetUsersRoles(user);
                if(roles.Contains(Roles.Admin) || roles.Contains(Roles.Manager))
                    return RedirectToAction("Index", "Products", new { area = "Admin" });
                return RedirectToAction("Index", "Products", new { area = "Client" });
            }

            return RedirectToAction("Login");
        }

        public IActionResult Register() => View();

        [HttpPost]
        public async Task<ActionResult> Register(LoginViewModel vm)
        {
            await _securityApplicationService.RegisterAndLogin(vm);

            return RedirectToAction("Index", "Products", new { area = "Client" });
        }

        public async Task<IActionResult> Logout()
        {
            await _securityApplicationService.Logout();
            return RedirectToAction(nameof(Login));
        }
    }
}