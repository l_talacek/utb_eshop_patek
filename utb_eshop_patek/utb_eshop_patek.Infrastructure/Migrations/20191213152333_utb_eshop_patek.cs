﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace utb_eshop_patek.Infrastructure.Migrations
{
    public partial class utb_eshop_patek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Amount",
                schema: "Web",
                table: "OrderItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                schema: "Web",
                table: "OrderItems",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                schema: "Web",
                table: "OrderItems");

            migrationBuilder.DropColumn(
                name: "Price",
                schema: "Web",
                table: "OrderItems");
        }
    }
}
