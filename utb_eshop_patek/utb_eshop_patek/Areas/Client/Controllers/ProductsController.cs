﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;
using utb_eshop_patek.Application.Admin.ApplicationServices.Security;

namespace utb_eshop_patek.Areas.Client.Controllers
{
    [Area("Client")]
    public class ProductsController : Controller
    {
        private readonly IProductApplicationService _productApplicationService;
        private readonly ISecurityApplicationService _securityApplicationService;

        public ProductsController(IProductApplicationService productApplicationService, ISecurityApplicationService securityApplicationService)
        {
            _productApplicationService = productApplicationService;
            _securityApplicationService = securityApplicationService;
        }

        public async Task<IActionResult> Index()
        {
            var vm = _productApplicationService.GetIndexViewModel();
            var user = await _securityApplicationService.GetCurrentUser(User);
            vm.UserID = user?.Id;
            vm.Login = user?.Email;

            return View(vm);
        }

        public IActionResult ProductIndex(int id)
        {
            var vm = _productApplicationService.GetProductViewModel(id);
            return View(vm);
        }
    }
}