﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Admin.ApplicationServices.Orders;
using utb_eshop_patek.Application.Admin.ApplicationServices.Security;
using utb_eshop_patek.Application.Client.ApplicationServices.Carts;
using utb_eshop_patek.Domain.Constants;

namespace utb_eshop_patek.Areas.Client.Controllers
{
    [Area("Client")]
    public class OrdersController : Controller
    {
        private readonly IOrderApplicationService _orderApplicationService;
        private readonly ISecurityApplicationService _securityApplicationService;

        public OrdersController(IOrderApplicationService orderApplicationService, ISecurityApplicationService securityApplicationService)
        {
            _orderApplicationService = orderApplicationService;
            _securityApplicationService = securityApplicationService;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _securityApplicationService.GetCurrentUser(User);
            var vm = _orderApplicationService.GetOrders(user.Id);

            return View(vm);
        }


        public async Task<IActionResult> Create()
        {
            var user = await _securityApplicationService.GetCurrentUser(User);
            _orderApplicationService.CreateOrder(user.Id, Request.Cookies[Cookies.UserTrackingCode]);

            return RedirectToAction(nameof(Index));
        }
    }
}