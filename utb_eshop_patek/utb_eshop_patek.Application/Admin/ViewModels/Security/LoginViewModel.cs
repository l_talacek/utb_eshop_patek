﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Admin.ViewModels.Security
{
    public class LoginViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
